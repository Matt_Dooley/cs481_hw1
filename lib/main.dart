import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to Flutter'),
        ),
        body: Center(
          child: Text('Hello World!\n\n'
              'Name: Matthew Dooley\n'
              'Graduation Date: May 21, 2021\n\n'
              '"When you reach the end of\n'
              'your rope, tie a knot in it and\n'
              'hang on."\n'
              '            -Franklin D. Roosevelt'),
        ),
      ),
    );
  }
}